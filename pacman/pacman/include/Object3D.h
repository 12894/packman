#pragma once

#include <iostream>
#include "GL\glew.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include <ShaderFuncs.h>
#include <glm/gtc/matrix_transform.hpp>
#include "glm/gtc/type_ptr.hpp"




class Object3D{
public:
	
	GLuint	vao, //Vertex Array Object
			vbo; //Vertex Buffer Object
	glm::mat4 transform;
	glm::mat4 translate;
	GLuint idTransform, colorID;
	void setup(std::vector<GLfloat>);
	void draw(glm::mat4 transform, glm::vec4 color);
	void setShaders(const char* vertexShader, const char *fragShader);
	int numVertex; //numero de vertices para este objeto


	GLuint shader; //referencia a los shaders

	//Object3D();

};



